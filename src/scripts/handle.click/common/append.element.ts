import { isElement } from 'lodash';
import { TAppendElement } from "../interface";

const appendElement: TAppendElement = (element: HTMLElement): void => {
    if(!isElement(element)) {
        throw new Error('Element is not valid'); 
    }
    const node = document.getElementById('main');
    node.appendChild(element);
}

export default appendElement;