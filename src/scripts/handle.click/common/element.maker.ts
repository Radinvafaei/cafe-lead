import { TElementMaker } from "../interface";

const elementMaker: TElementMaker = (inner: number): HTMLElement => {
    const div = document.createElement("SPAN");
    div.innerHTML = inner.toString();
    return div;
}

export default elementMaker;