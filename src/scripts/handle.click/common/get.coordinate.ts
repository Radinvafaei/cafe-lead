import { TGetCoordinate, ICoordinate } from "../interface";

const getCoordinate: TGetCoordinate = (event: MouseEvent): ICoordinate => {
    const { screenX, screenY, pageX, pageY, clientX, clientY } = event;
    return {
        client: {
            x: clientX,
            y: clientY,
        },
        page: {
            x: pageX,
            y: pageY,
        },
        screen: {
            x: screenX,
            y: screenY,
        },
    }
}

export default getCoordinate;