import appendElement from "./append.element";
import elementMaker from "./element.maker";
import getCoordinate from "./get.coordinate";
import isDivisible from "./is.divisible";
import setCoordToElement from "./set.coord.to.element";
import setColorByDivisibility from "./set.color.by.divisibility";

export {
    appendElement,
    elementMaker,
    getCoordinate,
    isDivisible,
    setCoordToElement,
    setColorByDivisibility,
}