import { isElement } from 'lodash'
import { TSetColorByDivisibility } from '../interface';

const setColorByDivisibility: TSetColorByDivisibility = (isDivisible : boolean, element: HTMLElement) : void => {
    if(!isElement(element)) {
        throw new Error('Element is not valid'); 
    }
    if(isDivisible){
        element.classList.add('divisibleElement')
    } else {
        element.classList.add('normalElement')
    }
}

export default setColorByDivisibility