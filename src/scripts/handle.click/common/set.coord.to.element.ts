import { isElement } from 'lodash'
import { TSetCoordToElement, ICoordinate } from "../interface";

const setCoordToElement: TSetCoordToElement = (element: HTMLElement, coords: ICoordinate): void =>  {
    const { client } = coords,
        { x, y } = client;
    if(!isElement(element)) {
        throw new Error('Element is not valid'); 
    }
    element.style.left = `${x}px`;
    element.style.top = `${y}px`;
}

export default setCoordToElement