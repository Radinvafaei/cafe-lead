import { getCoordinate, isDivisible, elementMaker, setCoordToElement, setColorByDivisibility, appendElement } from './common';
import { THandleClick } from './interface';

let countOfClick: number = 0

const handleClick: THandleClick = (event: MouseEvent): void => {

    countOfClick++;

    const coords = getCoordinate(event),
        divisible = isDivisible(countOfClick),
        newElement = elementMaker(countOfClick);
        setCoordToElement(newElement,coords);
        setColorByDivisibility(divisible,newElement);
        appendElement(newElement);
}

export default handleClick;