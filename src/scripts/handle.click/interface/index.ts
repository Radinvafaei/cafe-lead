interface ICoordinate {
    client: {
        x: number;
        y: number;
    };
    page: {
        x: number;
        y: number;
    };
    screen: {
        x: number;
        y: number;
    };
}

type THandleClick = (event: MouseEvent) => void

type TGetCoordinate = (event: MouseEvent) => ICoordinate

type TIsDivisnble = (countOfClicks: number, divisor?: number)=> boolean

type TElementMaker = (inner: number) => HTMLElement

type TAppendElement = (element: HTMLElement) => void

type TSetCoordToElement = (element: HTMLElement, coords: ICoordinate) => void

type TSetColorByDivisibility = (isDivisible : boolean, element: HTMLElement) => void;

export type {
    ICoordinate,
    THandleClick,
    TGetCoordinate,
    TIsDivisnble,
    TElementMaker,
    TAppendElement,
    TSetCoordToElement,
    TSetColorByDivisibility,
}