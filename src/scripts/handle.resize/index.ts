import { TGetRandomInt, THandleResize } from "./interface";

const getRandomInt: TGetRandomInt = (min: number,max: number): number => {
    return Math.floor(Math.random() * (max - min)) + min
}

const handleResize: THandleResize = (): void => {
    const { innerWidth, innerHeight } = window,
        elements: Array<HTMLElement> = Array.from(document.querySelectorAll('span'));

    for (const element of elements) {
        const newX = getRandomInt(60,innerWidth - 60),
            newY = getRandomInt(60,innerHeight - 60);
        
        element.style.left = `${newX}px`;
        element.style.top = `${newY}px`;
    }
}

export default handleResize