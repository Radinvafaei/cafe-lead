type TGetRandomInt = (min: number,max: number) => number;
type THandleResize = () => void;


export type{
    TGetRandomInt,
    THandleResize
}