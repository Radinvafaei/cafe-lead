import "../styles/style.scss"
import handleClick from "./handle.click"
import handleResize from './handle.resize'

document.addEventListener('click',(e) => handleClick(e))
window.addEventListener('resize',() => handleResize())