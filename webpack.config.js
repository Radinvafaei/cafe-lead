const path = require('path');

module.exports = {
    mode: 'development',
    entry: './src/scripts/index.ts',
    output: {
        filename: 'script.js',
        path: path.resolve(__dirname,'dist'),
        publicPath: "/dist/",
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.ts?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                  "style-loader",
                  "css-loader",
                  "sass-loader",
                ],
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
            },
        ],
    },
    resolve: {
        extensions: ['.ts','.js','.tsx'],
    },
    devServer: {
        static: path.join(__dirname, 'public/'),
        port:5000,
        hot: true,
    },
    
}